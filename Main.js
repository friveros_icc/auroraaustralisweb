var express = require('express');
var http = require('http');
var bodyParser = require('body-parser');

var app = express();
app.set('port', 8080);

// use domains for better error handling
app.use(function(req, res, next){
    // create a domain for this request
    var domain = require('domain').create();
    // handle errors on this domain
    domain.on('error', function(err){
        console.error('DOMAIN ERROR CAUGHT\n', err.stack);
        try {
            // failsafe shutdown in 5 seconds
            setTimeout(function(){
                console.error('Failsafe shutdown.');
                process.exit(1);
            }, 5000);

            // disconnect from the cluster
            var worker = require('cluster').worker;
            if(worker) worker.disconnect();

            // stop taking new requests
            server.close();

            try {
                // attempt to use Express error route
                next(err);
            } catch(error){
                // if Express error route failed, try
                // plain Node response
                console.error('Express error mechanism failed.\n', error.stack);
                res.statusCode = 500;
                res.setHeader('content-type', 'text/plain');
                res.end('Server error.');
            }
        } catch(error){
            console.error('Unable to send 500 response.\n', error.stack);
        }
    });

    // add the request and response objects to the domain
    domain.add(req);
    domain.add(res);

    // execute the rest of the request chain in the domain
    domain.run(next);
});

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));

// Agregamos las rutas
require('./Router')(app);


// 404 catch-all handler (middleware)
app.use(function(req, res, next) {
	res.status(404);
	res.type('text/plain');
	res.send('404 - Not Found');
});

var server;

function startServer() {
	server = http.createServer(app).listen(app.get('port'), function() {
		console.log('Express started in ' + app.get('port') +
			'; press Ctrl-C to terminate.');
	});
}

if(require.main === module)
	// Aplicación corre de manera directa.
	startServer(); 
else
	// Aplicación importada como módulo vía require
	module.exports = startServer;
