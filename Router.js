var registrosResource = require('./lib/resources/Registros')();
var registroResource = require('./lib/resources/Registro')();
var usuariosResource = require('./lib/resources/Usuarios')();

/**
 * Módulo que se encarga de registrar cada uno de los recursos
 * que componen el sistema.
 *
 * @author Francisco Riveros.
 * @email friveros.icc@gmail.com
 */
var Router = function(app) {

	// Establecemos las rutas.
	app.get('/', function(req, res, err) {
		res.redirect('/pages/listado');
	});	
	
	// Registros
	app.get('/api/registros', registrosResource.buscarRegistros);
	app.get('/api/registros/ultimo', registroResource.buscarUltimo);	
	// Usuarios
	app.get('/api/usuarios', usuariosResource.buscarUsuarios);
	app.post('/api/usuarios', usuariosResource.crearUsuario);
	app.put('/api/usuarios/:username', usuariosResource.actualizarUsuario);
	app.delete('/api/usuarios/:username', usuariosResource.eliminarUsuario);
	// Login
	app.post('/api/login', usuariosResource.login);
};

module.exports = Router;
