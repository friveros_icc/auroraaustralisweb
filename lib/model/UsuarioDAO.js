var mysql = require('mysql');
var _ = require('underscore');

/**
 * Módulo que se encarga del acceso a los datos de los `usuarios` del sistema.
 *
 * @author Francisco Riveros
 */
var UsuarioDAO = function() {	
	var _connection;

	_init();

	/**
	 * Inicializa la conexión con el servidor
	 */
	function _init() {
		_connection = mysql.createConnection({
			host 		: '127.0.0.1',
			user		: 'aurora_admin',
			password	: 'aurora_admin',
			database	: 'aurora',
			port 		: '3306'
		});
		_connection.connect();
	}
		
	/**
	 * Busca la password encriptada del usuario.
	 *
	 * @param `username` Nombre de usuario.
	 * @param `callback` Función 
	 */
	function buscarPasswordUsuario(username, callback) {
		var select	= 'SELECT   password '
                        	+ 'FROM     usuario '
				+ 'WHERE    username = ? ';
			
		_connection.query(select, [username], function(err, result) {
			var password = '';
			if(result.length > 0)
				password = result[0].password;
			callback(err, password);			
		});
		
	}

	/**
	 * Busca todos los usuarios que coinciden con los parámetros de búsqueda.
	 *
	 * @param `filtro` Objeto que contiene los campos por los cuales es posible filtrar
	 * el listado de `Usuarios`. El formato de este objeto es el siguiente.
	 * 
	 * {
	 * 	query
	 * 	inicio
	 * 	rango
	 * }
	 *
	 * @param `callback` Función.
	 */
	function buscarUsuarios(filtro, callback) {
		var select 	=	'SELECT     	us.username, us.nombre, us.apellido, us.email '
				+	'FROM       	usuario us '
				+	'WHERE 		us.username LIKE ?'
				+	'LIMIT		?, ?';		

		var values = ['%'+filtro.query+'%', filtro.inicio, filtro.rango];

		_connection.query(select, values, function(err, result) {			
			var usuarios = [];

			for(var i = 0; i < result.length; i++) {
				var usuario = {
					username 	: result[i].username,
					nombre 		: result[i].nombre,
					apellido	: result[i].apellido,
					email		: result[i].email
				};

				usuarios.push(usuario);
			}						

			callback(err, usuarios);
		});		
	}

	/**
	 * Verfica si es que el nombre de usuario ya existe en el sistema.
	 */
	function existeUsuario(username, callback) {
		var select =	'SELECT	username '
			   +	'FROM 	usuario '
			   +	'WHERE 	username = ? ';

		_connection.query(select, [username], function(err, result) {
			var existe = (result.length > 0) ? true : false;
			callback(err, existe);
		});	
	}

	/**
	 * Busca la cantidad de usuarios que coinciden con los parámetros de búsqueda.
	 *
	 * @param `filtro` Objeto que contiene los campos por los cuales es posible filtrar
	 * el listado de `Usuarios`. El formato de este objeto es el siguiente.
	 * 
	 * { query }
	 *
	 * @param `callback` Función.
	 */
	function cantidadUsuarios(filtro, callback) {
		var select 	=	'SELECT     	COUNT(1) AS cantidad '
				+	'FROM       	usuario us '
				+	'WHERE 		us.username LIKE ?';

		var values = ['%'+filtro.query+'%'];

		_connection.query(select, values, function(err, result) {			
			var cantidad = 0;

			if(result.length > 0) 
				cantidad = result[0].cantidad;

			callback(err, cantidad);
		});		
	}

	/**
	 * Crea un nuevo `usuario` en la base de datos.
	 *
	 * @param `usuario` Objeto con la información del nuevo usuario.
	 * {
	 * 	username
	 * 	password
	 * 	nombre
	 * 	apellido
	 * 	email
	 * }
	 *
	 * @param callback Función.
	 */
	function crearUsuario(usuario, callback) {
		var insert =	'INSERT INTO usuario '
			   +	'(username, password, nombre, apellido, email) '
			   +	'VALUES (?, ?, ?, ?, ?)';

		var values = [usuario.username, usuario.password, usuario.nombre, usuario.apellido, usuario.email];

		_connection.query(insert, values, function(err, result) { callback(err); });		
	}

	/**
	 * Actualiza la información del `usuario` en la base de datos.
	 *
	 * @param `usuario` Objeto con la información del usuario.
	 * {
	 * 	username
	 * 	password
	 * 	nombre
	 * 	apellido
	 * 	email
	 * }
	 *
	 * @param `callback` Función.
	 */
	function actualizarUsuario(usuario, callback) {
		var update =	'UPDATE usuario '
			   +	'SET 	password 	= ?, '
			   +		'nombre 	= ?, '
			   +		'apellido 	= ?, '
			   +		'email 		= ? '
			   +		'WHERE username = ? ';

		var values = [usuario.password, usuario.nombre, usuario.apellido,
		    usuario.email, usuario.username];

		_connection.query(update, values, function(err, resul) { callback(err); });
	}

	/**
	 * Elimina toda la información del usuario desde el sistema.
	 *
	 * @param `username` Nombre del usuario que se eliminará.
	 * @param `callback` Función.
	 */
	function eliminarUsuario(username, callback) {
		var remove = 	'DELETE FROM usuario WHERE username = ?';

		_connection.query(remove, [username], function(err, result) { callback(err); });
	
	}

	/**
	 * Busca el `password` encriptado del usuario en la base de datos.
	 *
	 * @param `username` Nombre de usuario de quién se desea su password.
	 * @param `callback` Función.
	 */
	function buscarPassword(username, callback) {
		var select = 'SELECT password FROM usuario WHERE username = ?';

		_connection.query(select, [username], function(err, resutl) {
			var password = '';

			if(result.length > 0)
				password = result[0].password;

			callback(err, password);
		});
	}

	return {				
		buscarUsuarios 		: buscarUsuarios,
		existeUsuario		: existeUsuario,
		cantidadUsuarios	: cantidadUsuarios,
		crearUsuario 		: crearUsuario,
		actualizarUsuario 	: actualizarUsuario,
		eliminarUsuario		: eliminarUsuario,
		buscarPasswordUsuario 	: buscarPasswordUsuario			
	};
}

module.exports = UsuarioDAO;

