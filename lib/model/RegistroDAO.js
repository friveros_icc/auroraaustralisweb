var mysql = require('mysql');
var _ = require('underscore');

/**
 * Módulo que se encarga del acceso a los datos de los registros de los
 * pesos de los lotes capturados en la cinta.
 *
 * @author Francisco Riveros
 */
var RegistroDAO = function() {	
	var _connection;

	_init();

	/**
	 * Inicializa la conexión con el servidor
	 */
	function _init() {
		_connection = mysql.createConnection({
			host 		: '127.0.0.1',
			user		: 'aurora_admin',
			password	: 'aurora_admin',
			database	: 'aurora',
			port 		: '3306'
		});
		_connection.connect();
	}

	/**
	 * Busca si existe un error registrado en el sistema.
	 */
	function _buscarMensajes(success) {
		var select 	= 	"SELECT IFNULL(( "
					+				"SELECT descripcion FROM error "
					+			"), '') AS error, "
					+			"IFNULL(( "
					+				"SELECT descripcion FROM exito "
					+			"), '') AS exito ";

		var values = [];

		_connection.query(select, values, function(err, result) {
			var mensajes = { error : '', exito : '' };

			if(result.length > 0) {
				mensajes.error = result[0].error;
				mensajes.exito = result[0].exito
			}

			success(mensajes);
		});
	}

	/**
	 * Busca la cantidad de los kilos totales procesados el día actual.
	 */
	function buscarTotalDia(success) {
		var select = 	'SELECT ROUND(SUM(peso)) peso '
				   +	'FROM lote_peso '
				   + 	'WHERE fecha_pesaje >= ? '
				   +	'AND fecha_pesaje <= ?';
		
		var fecha = new Date();
		var dd = fecha.getUTCDate();
		var mm = fecha.getUTCMonth()+1;
		var yy = fecha.getUTCFullYear();

		if(dd < 10) dd = '0' + dd;
		if(mm < 10) mm = '0' + mm;

		fecha = yy+'-'+mm+'-'+dd;

		var values = [fecha+' 00:00:00', fecha+'23:59:59'];
		console.log(values);
		_connection.query(select, values, function(err, result) {

			console.log(err);
			console.log(result);
			var peso = 0;
			if(result.length > 0)
				peso = result[0].peso;
			success(peso);			
		});
		
	}

	/**
	 * Busca el último registro de peso que se encuentra registrado.
	 */
	function buscarUltimoRegistro(success) {
		_buscarMensajes(function(mensajes) {
			// Si no existen errores registrados, buscamos la información del registro.			
			var select 	= 	'SELECT 	lp.codigo_lote, lp.peso, '
					+				'DATE_FORMAT(lp.fecha_pesaje,  \'%d-%m-%Y %H:%i:%s\') AS fecha_pesaje, '
					+				'l.nombre_productor, l.tara_total, '
					+				'ROUND(( '
					+						'SELECT 	SUM(lt.peso) '
					+						'FROM 		lote_peso lt '
					+						'WHERE 		lt.codigo_lote = l.codigo_lote '
					+				'), 1) peso_acomulado '
					+	'FROM 		lote_peso lp, lote l '
					+	'WHERE 		lp.codigo_lote = l.codigo_lote '
					+	'ORDER BY 	lp.fecha_pesaje '
					+	'DESC LIMIT 1';

			var values = [];

			_connection.query(select, values, function(err, result) {			
				var registro;
				
				if(result.length === 0) {
					registro = {
						codigoLote 	: '0',
						peso 		: '0.0',
						fecha 		: '00-00-0000 00:00:00',
						productor 	: '0',
						taraTotal 	: '0',
						pesoTotal 	: '0',
						error 		: error
					};
				} else {
					registro = {
						codigoLote 	: result[0].codigo_lote,
						peso 		: result[0].peso,
						fecha 		: result[0].fecha_pesaje,
						productor 	: result[0].nombre_productor,
						taraTotal 	: result[0].tara_total,
						pesoTotal 	: result[0].peso_acomulado,
						error 		: mensajes.error,
						exito		: mensajes.exito
					};			
				}

				success(err, registro);
			});			
		});			
	}

	/**
	 * Busca todos los registro.
	 */
	function buscarRegistros(filtro, success) {
		var select 	=	'SELECT 	l1.codigo_lote, SUM(l1.peso) AS peso, '
				+ 				'DATE_FORMAT(lt.fecha_ultimo_pesaje, \'%d-%m-%Y %H:%i:%s\') AS fecha_pesaje, '
				+				'lt.codigo_productor, '
				+				'lt.nombre_productor, '
				+				'lt.tara_total '
				+ 	'FROM 		lote_peso 	l1, '
				+			'lote 		lt ' 
				+ 	'WHERE 		l1.codigo_lote 		LIKE ? '
				+	'AND 		lt.codigo_lote 	= l1.codigo_lote '
				+	'AND 		( '
				+ 					'lt.codigo_productor LIKE ? '
				+					'OR lt.nombre_productor LIKE ? '
				+				') ';
		
		if(!_.isUndefined(filtro.desde))
			select 	+=	'AND 		lt.fecha_ultimo_pesaje >= ? ';
		if(!_.isUndefined(filtro.hasta))
			select 	+=	'AND 		lt.fecha_ultimo_pesaje <= ? ';

		select 		+= 	'GROUP BY 	l1.codigo_lote '
				+ 	'ORDER BY 	lt.fecha_ultimo_pesaje DESC '
				+ 	'LIMIT 		?, ?';		

		var values = [	'%'+filtro.codigoLote+'%', 
				'%'+filtro.productor+'%', 
				'%'+filtro.productor+'%'];

		console.log(select);
		if(!_.isUndefined(filtro.desde))
			values.push(filtro.desde);
		if(!_.isUndefined(filtro.hasta))
			values.push(filtro.hasta);
		values.push(filtro.inicio); 
		values.push(filtro.rango);

		_connection.query(select, values, function(err, result) {			
			var registros = [];

			for(var i = 0; i < result.length; i++) {
				var registro = {
					codigoLote 		: result[i].codigo_lote,
					peso 			: result[i].peso,
					fecha 			: result[i].fecha_pesaje,
					codigoProductor 	: result[i].codigo_productor,
					productor 		: result[i].nombre_productor,
					taraTotal		: result[i].tara_total
				};

				registros.push(registro);
			}						

			success(err, registros);
		});		
	}

	/**
	 * Busca la cantidad de registros que coinciden con
	 * el filtro de búsqueda.
	 */
	function cantidadRegistros(filtro, success) {		
		var select 	= 	'SELECT 	COUNT(1) AS cantidad '
				+	'FROM ( '
				+		'SELECT		lt.codigo_lote '
				+ 		'FROM 		lote 		lt, '
				+				'lote_peso 	lp '
				+ 		'WHERE 		lt.codigo_lote LIKE ? '
				+		'AND 		lp.codigo_lote = lt.codigo_lote '		
				+		'AND 		( '
				+ 					'lt.codigo_productor LIKE ? '
				+					'OR lt.nombre_productor LIKE ? '
				+				') ';

		if(!_.isUndefined(filtro.desde))
			select 	+=		'AND 		lt.fecha_ultimo_pesaje >= ? ';
		if(!_.isUndefined(filtro.hasta))
			select 	+=		'AND 		lt.fecha_ultimo_pesaje <= ? ';

		select		+=		'GROUP BY	lt.codigo_lote '
				+	') AS lt ';
		

		var values = [	'%'+filtro.codigoLote+'%', 
				'%'+filtro.productor+'%', 
				'%'+filtro.productor+'%'];

		if(!_.isUndefined(filtro.desde))
			values.push(filtro.desde);
		if(!_.isUndefined(filtro.hasta))
			values.push(filtro.hasta);		
		
		_connection.query(select, values, function(err, result) {			
			var cantidad = result[0].cantidad;

			success(err, cantidad);
		});
	}

	return {				
		buscarUltimoRegistro 	: buscarUltimoRegistro,
		buscarRegistros 		: buscarRegistros,
		cantidadRegistros 		: cantidadRegistros,
		buscarTotalDia 			: buscarTotalDia
	};
}

module.exports = RegistroDAO;

