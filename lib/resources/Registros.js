var RegistroDAO = require('../model/RegistroDAO');
var _ = require('underscore');
var nodeExcel = require('excel-export');


/**
 * Módulo que representa al recurso "Registros", encargado 
 * de gestionar las peticiones del cliente.
 * 
 * @author Francisco Riveros.
 */
var Registros = function() {
	var _registroDAO = RegistroDAO();		

	/**
	 * Genera el listado de registros en formato JSON
	 */
	function _generarListadoJson(res, filtro, representacion) {
		if(representacion.total === 0) {
			representacion.lotes = [];
			res.status(204).json(representacion);				
		} else {
			// Verificamos si están solicitando la última página.
			if(filtro.ultimaPagina == 'true') 
				filtro.inicio = _calcularIndicesUltimaPagina(filtro.rango, representacion.total);

			console.log(filtro);

			_registroDAO.buscarTotalDia(function(pesoTotal) {
				representacion.pesoTotal = pesoTotal;
				_registroDAO.buscarRegistros(filtro, function(err, registros) {	
					if(err) return next(err);
				
					representacion.lotes = registros;
					res.status(200).json(representacion);					
				});
			});
		}
	}

	/**
	 * Genera el listado de registros en formato EXCEL
	 */
	function _generarListadoExcel(res, filtro, representacion) {
		filtro.inicio = 0;
		filtro.rango = representacion.total;

		_registroDAO.buscarRegistros(filtro, function(err, registros) {	
			if(err) return next(err);

			var conf = {};			
			conf.cols = [{
				caption : 'CÓDIGO PRODUCTOR',
				type 	: 'string'
			}, {
				caption : 'PRODUCTOR',
				type 	: 'string'
			}, {
				caption : 'CÓDIGO LOTE', 
				type 	: 'string'			
			}, {
				caption	: 'FECHA ÚLTIMO REGISTRO',
				type 	: 'string'/*,
				beforeCellWrite : function(row, cellData) {
					var originDate = new Date(Date.UTC(1899, 11, 30));
					return (cellData - originDate) / (24 * 60 * 60 * 1000);
				}*/
			}, { 
				caption : 'PESO TOTAL', 
				type 	: 'number'
			}, { 
				caption : 'TARA TOTAL', 
				type 	: 'number'
			}, { 
				caption : 'PESO NETO TOTAL', 
				type 	: 'number'
			}];

			conf.rows = [];

			_.each(registros, function(registro) {
				var array = [	registro.codigoProductor, 
								registro.productor,
								registro.codigoLote, 
								//new Date(registro.fecha * 1000), 
								registro.fecha,
								registro.peso,
								registro.taraTotal,
								(registro.peso - registro.taraTotal)];
				conf.rows.push(array);
			});

			var result = nodeExcel.execute(conf);
			res.setHeader('Content-Type', 'application/vnd.openxmlformats');
			res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
			res.end(result, 'binary');
		});
	}	

	/**
	 * Calcula el índice de inicio de paginación si es 
	 * que se esta solicitando la última página.
	 */
	function _calcularIndicesUltimaPagina(rango, total) {
		var inicio = (rango === total) ? 0 : parseInt(total / rango) * rango;

		if(inicio === total)
			return inicio - rango;
		return inicio;
	}

	/** 
	 * Método que se encarga de buscar todos los registros
	 * en la base de datos.
	 */
	function buscarRegistros(req, res, next) {	
		var representacion = {};
		var desde;
		var hasta;
		
		if(req.query.desde.length === 0) 
			desde = undefined;
		else
			desde = ''.concat(req.query.desde.split('-')[2], '-' 
					, req.query.desde.split('-')[1], '-'
					, req.query.desde.split('-')[0], ' 00:00:00');		

		if(req.query.hasta.length === 0) 
			hasta = undefined;
		else
			hasta 	= ''.concat(req.query.hasta.split('-')[2], '-' 
					+ req.query.hasta.split('-')[1], '-'
					+ req.query.hasta.split('-')[0], ' 23:59:59');

		var filtro = {
			inicio		: parseInt(req.query.inicio),
			rango		: parseInt(req.query.rango),
			codigoLote 	: req.query.codigoLote,
			productor 	: req.query.productor, 
			desde 		: desde,
			hasta 		: hasta,
			ultimaPagina	: req.query.ultimaPagina
		};			

		_registroDAO.cantidadRegistros(filtro, function(err, cantidad) {
			if(err) return next(err);

			representacion.total = cantidad;

			if(req.accepts('application/json'))
				_generarListadoJson(res, filtro, representacion);
			if(req.accepts('application/vnd.ms-excel'))
				_generarListadoExcel(res, filtro, representacion);
		});	
	}

	return {
		buscarRegistros : buscarRegistros
	};
};

module.exports = Registros;
