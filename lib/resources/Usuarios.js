var UsuarioDAO = require('../model/UsuarioDAO');
var bcrypt = require('bcrypt-nodejs');


/**
 * Módulo que representa al recurso `Usuarios`, encargado 
 * de gestionar las peticiones del cliente.
 * 
 * @author Francisco Riveros.
 */
var Usuarios = function() {
	var _usuarioDAO = UsuarioDAO();		

	/**
	 * Calcula el índice de inicio de paginación si es 
	 * que se esta solicitando la última página.
	 */
	function _calcularIndicesUltimaPagina(rango, total) {
		var inicio = (rango === total) ? 0 : parseInt(total / rango) * rango;

		if(inicio === total)
			return inicio - rango;
		return inicio;
	}

	/** 
	 * Método que se encarga de bucar los usuarios que se encuentran
	 * registrados en el sistema.
	 */
	function buscarUsuarios(req, res, next) {	
		var representacion = {};
		
		var filtro = {
			inicio		: parseInt(req.query.inicio),
			rango		: parseInt(req.query.rango),
			query 		: req.query.query,
			ultimaPagina	: req.query.ultimaPagina
		};			

		_usuarioDAO.cantidadUsuarios(filtro, function(err, cantidad) {
			if(err) return next(err);

			representacion.total = cantidad;
			if(representacion.total === 0) {
				representacion.lotes = [];
				res.status(204).json(representacion);				
			} else {
				// Verificamos si están solicitando la última página.
				if(filtro.ultimaPagina == 'true') 
					filtro.inicio = _calcularIndicesUltimaPagina(filtro.rango, representacion.total);

				console.log(filtro);

				_usuarioDAO.buscarUsuarios(filtro, function(err, usuarios) {	
					if(err) return next(err);
				
					representacion.usuarios = usuarios;
					res.status(200).json(representacion);					
				});
			}
		
		});	
	}

	/**
	 * Método que crea un nuevo `usuario` en el sistema.
	 */
	function crearUsuario(req, res, next) {
		var usuario = req.body;
		/*var usuario = {
			username 	: 'yaz',
			password 	: 'friveros',
			nombre		: 'yazmin',
			apellido 	: 'vega',
			email 		: 'yaz@vega.cl'
		};*/

		usuario.password = bcrypt.hashSync(usuario.password, bcrypt.genSaltSync(), null);


		_usuarioDAO.existeUsuario(usuario.username, function(err, existe) {
			if(err) return next(err);
			
			if(!existe) {
				_usuarioDAO.crearUsuario(usuario, function(err) {
					if(err) return next(err);

					res.status(200).json({ msg : 'usuario creado con éxito' });
				});
			}
		});
	}

	/**
	 * Método que actualizar un `usuario` del sistema.
	 */
	function actualizarUsuario(req, res, next) {
		var usuario = req.body;
		usuario.username = req.params.username;
		
		/*var usuario = {
			username 	: 'yaz',
			password 	: 'friveros',
			nombre		: 'yazmin',
			apellido 	: 'vega',
			email 		: 'yaz@vega.cl'
		};*/

		usuario.password = bcrypt.hashSync(usuario.password, bcrypt.genSaltSync(), null);

		_usuarioDAO.actualizarUsuario(usuario, function(err) {
			if(err) return next(err);

			res.status(200).json({ msg : 'usuario actualizado con éxito' });
		});
	}

	/**
	 * Método que elimina un `usuario` del sistema.
	 */
	function eliminarUsuario(req, res, next) {
		var username = req.params.username;

		_usuarioDAO.eliminarUsuario(username, function(err) {
			if(err) return next(err);

			res.status(200).json({ msg : 'usuario eliminado con éxito' });

		});	
	}

	/**
	 * Método que se encarga de realizar el proceso de validación del login
	 * de usuario
	 */
	function login(req, res, next) {
		var usuario = res.body;
		/*var usuario = {
			username : 'yaz',
			password : 'friveros1'
		};*/

		_usuarioDAO.buscarPasswordUsuario(usuario.username, function(err, password) {
			if(bcrypt.compareSync(usuario.password, password))
				res.status(200).json({msg : 'usuario válido'});
			else
				res.status(500).json({msg : 'El usuario o contraseña no son correctos'});
		});
	}

	return {
		buscarUsuarios 		: buscarUsuarios,
		crearUsuario 		: crearUsuario,
		login 			: login,
		actualizarUsuario	: actualizarUsuario,
		eliminarUsuario		: eliminarUsuario
	};
};

module.exports = Usuarios;
