var RegistroDAO = require('../model/RegistroDAO');
var _ = require('underscore');

/**
 * Módulo que representa al recurso "Registro", encargado 
 * de gestionar las peticiones del cliente.
 * 
 * @author Francisco Riveros.
 */
var Registro = function() {
	var _registroDAO = RegistroDAO();

	/**
	 * Método que se encarga de buscar el último registro
	 * en la base de datos.
	 */
	function buscarUltimo(req, res, next) {
		_registroDAO.buscarUltimoRegistro(function(err, registro) {
			if(err) return next(err);		
			
			res.status(200).json(registro);			
		});
	}	

	return {
		buscarUltimo : buscarUltimo
	};
};

module.exports = Registro;