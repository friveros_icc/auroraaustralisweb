require.config({
	waitSeconds : 200,
	// By default load any module IDs from js/lib
	baseUrl	: '/js/lib',
	paths	: {
		app	: '../app',
		tpl : '/template'
	},
	shim 	: {		
		'bootstrap'					: ['jquery'],	
		'jquery-toolbar'			: ['jquery'],
		'chosen.jquery'				: ['jquery'],
		'bootstrap-datetimepicker'	: ['jquery', 'bootstrap']
	}
});

require([
	'order!jquery', 
	'order!underscore', 
	'order!domReady', 
	'order!jquery-toolbar', 
	'order!bootstrap', 
	'order!chosen.jquery', 
	'order!bootstrap-datetimepicker',
	MainModule.ruta], 
	function($, _, domReady, 
		Toolbar, Bootstrap, Chosen, DateTimePicker,
		MainModuleController) {

	domReady(function() {		
		var mainModuleController = MainModuleController();		

		Date.prototype.toString = function() {			
			return this.toDate() + ' ' + this.toTime();
		};

		Date.prototype.toTime = function() {			
			var hh = this.getUTCHours();
			var mi = this.getUTCMinutes();
			var ss = this.getUTCSeconds();

			if(hh < 10)
				hh = '0' + hh;
			if(mi < 10)
				mi = '0' + mi;
			if(ss < 10)
				ss = '0' + ss;
			return hh + ':' + mi + ':' + ss;
		};

		Date.prototype.toDate = function() {
			var dd = this.getUTCDate();
			var mm = this.getUTCMonth() + 1;
			var yy = this.getUTCFullYear();
			
			if(dd < 10)
				dd = '0' + dd;
			if(mm < 10)
				mm = '0' + mm;			
			return dd + '-' + mm + '-' + yy;
		};


		String.prototype.toTimeStamp = function() {
			var year = this.split('-')[2];
			var month = this.split('-')[1];
			var day = this.split('-')[0];
			var date = new Date(year, parseInt(month) - 1, day);
			return Math.round(date.getTime());
		};			
	});
});