define(['FileSaver'], function(FileSaver) {
	/**
	 * Módulo que se encarga de acceder a la información de los lotes 
	 * que se encuentran en el servidor.
	 *
	 * @author Francisco Riveros
	 * @email friveros.icc@gmail.com
	 */
	var LoteModel = function() {
		var _url = window.location.href.split( '/' )[0]+'//'+window.location.href.split( '/' )[2]+'/api/registros';

		/**
		 * Busca la información de los lotes que se encuentran en el sistema.
		 */
		function buscar(filtro, success) {			
			$.ajax({
				beforeSend 	: function(req){					
					req.setRequestHeader("Accept","application/json");
				},
				type 		:'GET',
				url 		: _url,				
				dataType 	: 'json',
				data 		: filtro,
				success 	: function(data, textStatus, xhr) { 												
					if(xhr.status == 204)
						data = { 
						total : 0,
						lotes : []
					};					
					success(data); 
				}
			});
		}

		/**
		 * Busca la información de los registros de pesos de lotes
		 * en formato excel.
		 */
		function buscarExcel(filtro, success) {	
			var fileName = "registroPesos.xlsx";			
			var query = '?';
			for(var element in filtro)
				query += element+'='+filtro[element]+'&';

			var oReq = new XMLHttpRequest();
			oReq.open("GET", _url+query, true);			
			oReq.setRequestHeader("Accept", "application/vnd.ms-excel");
			oReq.responseType = "blob";

			oReq.onload = function(oEvent) {								
				var bb = new Blob([oReq.response], { type : "application/vnd.ms-excel" });				
				saveAs(bb, fileName);

				success();
			}
				
			oReq.send();
		}

		/**
		 * Busca la información del último registro capturado.
		 */
		function buscarUltimo(success) {			
			$.getJSON(_url+'/ultimo', function(data) { success(data); });
		}

		return {
			buscar 			: buscar,
			buscarExcel 	: buscarExcel,
			buscarUltimo 	: buscarUltimo
		};
	};


	return LoteModel;
});