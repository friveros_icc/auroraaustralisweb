define(['app/controller/sistema/MainController',
		'app/model/LoteModel',
		'text!tpl/registroActual/dashboard.html'], 
	function(MainController, LoteModel, TplDashboard) {

		/**
		 * Módulo que se encarga de controlar la interfaz que despliega 
		 * la información capturada en tiempo real desde la línea
		 *
		 * @author Francisco Riveros.
		 * @email friveros.icc@gmail.com
		 */
		var RegistroActualController = function() {
			var _mainController;
			var _loteModel;
			var _tplDashboard;			

			_init();

			/**
			 * Inicializa las variables globales.
			 */
			function _init() {
				_mainController = MainController();
				_loteModel = LoteModel();
				_tplDashboard = _.template(TplDashboard);				
				
				_buscarUltimo();
				_busquedaContinua();

				// Ocultamos el título de la vista				
				$('#header').hide();				
			}

			/**
			 * Función que es ejecutada de forma indefinida para actualizar
			 * la información de los últimos pesos capturados.
			 */
			function _busquedaContinua() {
				setTimeout(function() {					
					_buscarUltimo();
					_busquedaContinua();
					
				}, 1500);				
			}

			/**
			 * Busca en el servidor la información del último registro
			 * capturado.
			 */
			function _buscarUltimo() {
				_loteModel.buscarUltimo(function(data) {					
					data.peso = data.peso.toString().split('');
					data.codigoLote = data.codigoLote.toString().split('');
					data.pesoTotal = data.pesoTotal.toString().split('');
					data.taraTotal = data.taraTotal.toString().split('');

					if(data.taraTotal[data.taraTotal.length-2] !== '.') {
						data.taraTotal.push('.');
						data.taraTotal.push('0');
					}

					if(data.peso[data.peso.length-2] !== '.') {
						data.peso.push('.');
						data.peso.push('0');
					}

					if(data.pesoTotal[data.pesoTotal.length-2] !== '.') {
						data.pesoTotal.push('.');
						data.pesoTotal.push('0');
					}

					$('#mainList').html(_tplDashboard({ registro : data }));
					$('#contenedorGlobal').css({'background-color' : '#000000'});
					_mainController.closeLoading();																
				});
			}			
		};

		return RegistroActualController;
});
