define(['app/controller/sistema/MainController', 
		'app/controller/sistema/BuscadorController', 
		'app/model/LoteModel',
		'text!tpl/listadoLotes/formBusquedaAvanzada.html',
		'text!tpl/listadoLotes/listado.html'], 
	function(	MainController, 
				BuscadorController, 
				LoteModel,
				TplBusquedaAvanzada, 
				TplListado) {

	/**
	 * Módulo encargado de administrar la vista principal de listado 
	 * de lotes.
	 *
	 * @author Francisco Riveros.
	 * @email friveros.icc@gmail.com
	 */
	var ListadoLotesController = function() {
		var _mainController;
		var _buscadorController;
		var _loteModel;
		var _tplListado;

		_init();

		/**
		 * Inicializa las variables globales y los módulos que
		 * utiliza este módulo.
		 */
		function _init() {
			_mainController = MainController();
			_loteModel = LoteModel();
			_tplListado = _.template(TplListado);

			// Inicializamos el controlador del buscador.
			_buscadorController = BuscadorController({
				contenedor 				: 'mainForm',
				contenidoAvanzada 		: _.template(TplBusquedaAvanzada)(),
				funcionBuscarElementos 	: _buscarLotes,
				eventosAvanzada 		: _eventosBusquedaAvanzada,
				showBusquedaAvanzada 	: true,
				estados 				: []
			});								
			
			_buscadorController.showView();
			_buscadorController.search();			
			_enlazarEventos();

			// Título de la vista
			$('.head span').html('Registros');
		}

		/**
		 * Función que es ejecutada por el "BuscadorController" al momento
		 * de que son presionados los botones de búsqueda.
		 */
		function _buscarLotes(filtro) {			
			_mainController.openLoading();

			if(!_.isUndefined(filtro.query))
				filtro.codigoLote = filtro.query;

			_loteModel.buscar(filtro, function(data) {
				$('#mainList').html(_tplListado({ lotes : data.lotes }));
				_buscadorController.setTotalPaginador(data.total);

				_mainController.closeLoading();
			});
		}

		/**
		 * Enlaza el evento click sobre el botón de exportar a Excel.
		 */
		function _enlazarEventos() {
			$('#btnExportarElemento').unbind('click');
			$('#btnExportarElemento').bind('click', _descargarExcel);
		}

		/**
		 * Se encarga de descargar el archivo Excel.
		 */
		function _descargarExcel() {
			_mainController.openLoading();

			var filtro = _buscadorController.getFiltro();
			filtro.inicio = 0;
			filtro.rango = 15;

			_loteModel.buscarExcel(filtro, function() {
				_mainController.closeLoading();
			});
		}

		/**
		 * Enlaza los eventos sobre el formulario de búsqueda avanzada.
		 */
		function _eventosBusquedaAvanzada() {
			$('.calendar').datetimepicker();
		}

		return {};
	};

	return ListadoLotesController;	
});
