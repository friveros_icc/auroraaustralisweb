define(function() {
	/**
	 * Módulo encargado de administrar las variables del paginador
	 * de datos.
	 *
	 * @author Francisco Riveros.
	 */
	var Paginador = function(start, range, total) {
		var _total;
		var _range;
		var _start;
		var _end;

		init(start, range, total);

		/**
		 * Inicializa las variables globales
		 */
		function init(start, range, total) {
			_total = total;
			_range = range;
			_start = start;

			if(_total < _range)
				_end = _total;
			else
				_end = _range;
		}

		/**
		 * Establece la cantidad total de elementos que existen.
		 */
		function setTotal(total) {
			_total = total;

			if(_total === 0) {
				_start = 0;
				_end = 0;
			}

			if(_range <= _total && _end === 0) {
				_end = _range;
			} else {
				if(_end > _total || _end === 0)
					_end = _total;
				if(_range >= _total && (_end - _range) < _total)
					_end = _total;
			}
		}

		/**
		 * Avanza el indice de inicio en la cantidad del rango del paginador
		 */
		function next() {			
			_start += _range;
			_end += _range;			

			if(_start >= _total)
				if(_total === 0)
					_start = 0;
				else
					_start -= _range;

			if(_end > _total)
				_end = _total;
		}

		/**
		 * Retrocede el indice de inicio en la cantidad del rango del paginador.
		 */
		function previous() {
			var aux = _end - _start;
			_start -= _range;

			if(aux < _range)
				_end -= aux;
			else 
				_end -= _range;
            
			if(_start < 0)
				_start = 0;
            
			if(_end > 0) {
				if(_end < _range) {
					if(_total == 0)
						_end = 0;
					else
						_end = _range;
				}
			} else {
				if(_total < _range)
					_end += _total;
				else
					_end += _range;
			}
		}

		function last() {
			_start = Math.floor(_total / _range) * _range;
			_end = _total;

		}

		/**
		 * Devuelve la etiqueta con el formato "A - B de C"
		 */
		function label() {
			var aux;
			if((_start + 1) > _total)
				aux = _total;
			else
				aux = _start + 1;

			return aux + ' - ' + _end + ' de ' + _total;
		}

		function getStart() {
			return _start;
		}

		function getRange() {
			return _range;
		}

		function getEnd() {
			return _end;
		}

		function getTotal() {
			return _total;
		}

		return {
			init 		: init,
			setTotal 	: setTotal,
			next 		: next,
			previous 	: previous,
			last 		: last,
			label 		: label,
			getStart	: getStart,
			getRange 	: getRange,
			getEnd 		: getEnd,
			getTotal 	: getTotal
		};
	};

	return Paginador;
});