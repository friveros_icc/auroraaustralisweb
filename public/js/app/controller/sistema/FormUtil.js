define(function() {
	/**
	 * Módulo que permite obtener y establecer los valores de los formularios.
	 *
	 * @author Cristofer Herrera
	 * @author Francisco Riveros
	 *
	 */
	var FormUtil = (function() {

		/** 
		 * Busca todos los elementos del tipo input dentro 
		 * de un elemento del DOM.
		 */
		function _listInput(idForm) {
			var list = [];
			var inputs = $(idForm).find('input');

			for(var i=0; i<inputs.length; i++) {
				var input = inputs[i];

				if(_isInput(input))
					list.push(input);
			}

			return list;
		}

		/** 
		 * Busca todos los elementos del tipo select dentro 
		 * de un elemento del DOM.
		 */
		function _listSelect(idForm) {
			var list = [];
			var inputs = $(idForm).find('select');

			for(var i=0; i<inputs.length; i++) {
				var input = inputs[i];

				if(_isSelect(input))
					list.push(input);
			}

			return list;
		}

		/** 
		 * Busca todos los elementos del tipo radio dentro 
		 * de un elemento del DOM.
		 */
		function _listRadio(idForm) {
			var list = [];
			var inputs = $(idForm).find('input');

			for(var i=0; i<inputs.length; i++) {
				var input = inputs[i];

				if(_isRadio(input))
					list.push(input);
			}

			return list;
		}

		/**
		 * Se comprueba que los elementos posean el formato de información creada en el formulario
		 * @element: es una etiqueta HTML, un campo del formulario.
		 * el id del input comienza con txt : 		Ej: txt-nombrePropiedad
		 */
		function _isInput(element) {			
			if(!_.isUndefined($(element).attr('id')) && $(element).attr('id').substring(0, 3) === 'txt')
				return true;
			return false;
		}

		/**
		 * Se comprueba que los elementos posean el formato de información creada en el formulario
		 * el id del selector comienza con slc :	Ej: slc-nombrePropiedad
		 */
		function _isSelect(element) {			
			if(!_.isUndefined($(element).attr('id')) && $(element).attr('id').substring(0, 3) === 'slc')
				return true;
			return false;
		}
		 
		/**
		 * Se comprueba que los elementos posean el formato de información creada en el formulario
		 * el nombre del radio comienza con rbt :	Ej: rbt-nombrePropiedad
		 */
		function _isRadio(element) {			
			if(!_.isUndefined($(element).attr('id')) && $(element).attr('id').substring(0, 3) === 'rbt')
				return true;
			return false;
		}

		/**
		 * @param idForm Identificador del elemento del DOM que contiene
		 * el formulario.
		 */
		function getValues(idForm) {
			var object = {};
			var inputs = _listInput(idForm);
			var selects = _listSelect(idForm);
			var radios = _listRadio(idForm);

			for(var i = 0; i < inputs.length; i++) {
				var $input = $(inputs[i]);
				var attr = $input.attr('id').substring(4);
				var val = $input.val();

				object[attr] = val;
			}

			for(var i = 0; i < selects.length; i++) {
				var $select = $(selects[i]);
				var attr = $select.attr('id').substring(4);
				var val = $select.val();

				object[attr] = val;
			}

			for(var i = 0; i < radios.length; i++) {
				var $radio = $(radios[i]);
				var attr = $radio.attr('name').substring(4);
				var val = $('input[name="'+$radio.attr('name')+'"]:checked').val();

				object[attr] = val;
			}			

			return object;
		}

		/**
		 * Método que establece los valores por defecto de un formulario.
		 *
		 * @param idForm id del elemento del DOM o de un template. normalmente un Formulario
		 * @param values Objeto con los valores del formulario.
		 *
		 */	
		function setValues(idForm, values) {
			var inputs = _listInput(idForm);
			var selects = _listSelect(idForm);
			var radios = _listRadio(idForm);

			for(var i = 0; i < inputs.length; i++) {
				var $input = $(inputs[i]);
				attr = $input.attr('id').substring(4);

				if(!_.isUndefined(values[attr]))
					$input.val(values[attr]);
			}

			for(var i = 0; i < selects.length; i++) {
				var $select = $(selects[i]);
				attr = $select.attr('id').substring(4);
				$select.val(values[attr]);
			}

			for(var i = 0; i < radios.length; i++) {
				var $radio = $(radios[i]);
				attr = $radio.attr('name').substring(4);
				
				if($radio.attr('value') === values[attr].toString())
					$radio.prop('checked',true);
			}

			$(idForm+' .chosen-select').trigger("chosen:updated");
			
		}

		return {
			getValues : getValues,
			setValues : setValues
		}
	})();

	return FormUtil;
});