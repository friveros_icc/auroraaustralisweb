define(['app/controller/sistema/MenuController', 'text!tpl/sistema/baseTemplate.html'], 
	function(MenuController, TplBase) {
	/**
	 * Módulo que se encarga de administrar las características básicas de
	 * la vista.
	 *
	 * @author Francisco Riveros.
	 */
	var MainController = function() {
		var _menuController = MenuController();	
		var _tplBase = _.template(TplBase);

		_init();
		
		// Función que se ejecuta al momento de cargar de ejecutar al módulo.
		function _init() {			
			// Cargamos el template base
			$('#overlay').after(_tplBase());

			_menuController.showView();
			_monitoriarAlto();
			
			$(window).resize(function() {
				$('#mainMenu').css('height', $(window).height());
				$('#mainList').css('height', _getAltoMaximo());
			});
		}

		// Busca el alto que posee la ventana del navegador.
		function _getAltoMaximo() {
			return $(window).height() - parseInt($('#mainForm').css('height')) - parseInt($('#header').css('height'));
		}

		// Se encarga de monitoriar si ha cambiado la estructura de la vista para reajustar
		// la altura de esta.
		function _monitoriarAlto() {
			var target = $('#mainForm')[0];

			if(!_.isUndefined(MutationObserver) && !_.isNull(MutationObserver)) {
				var observer = new MutationObserver(function(mutations) {
					$('#mainList').css('height', _getAltoMaximo());
				});

				var config = {
					attributes 		: true,
					childList 		: true,
					characterData 	: true
				};

				observer.observe(target, config);
			} else {
				$('#mainList').css('height', _getAltoMaximo());
				setTimeout(function() {
					_monitoriarAlto();
				}, 5);
			}
		}

		// Método que se encarga de cerrar la visualización de cargando.
		function closeLoading() {		
			$('#overlay').fadeOut(500);
		}

		// Lanza la visualización de cargando información.
		function openLoading() {
			$('#overlay').fadeIn(500);
		}

		return {
			closeLoading 	: closeLoading,
			openLoading 	: openLoading
		};
	};

	return MainController;
});