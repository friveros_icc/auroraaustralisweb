define(['text!tpl/sistema/menu.html'], function(TplMenu) {

	/**
	 * Clase que se encarga de administrar el menú del sistema.
	 *
	 * @author Francisco Riveros.
	 */
	var MenuController = function() {		
		// Obtenemos la URL de la vista actual.
		var _urlActiva;
		var _tplMenu;
		var _application;	

		_init();

		/**
		 * Inicializa las variables globales.
		 */
		function _init() {
			_urlActiva = window.location.pathname.substring(0, window.location.pathname.length-1);
			_tplMenu = _.template(TplMenu);
			_application = [{
				idModulo 	: '1',
				icon 		: 'glyphicon glyphicon-list',
				url 		: '/pages/listado'
			}, {
				idModulo 	: '2',
				icon 		: 'glyphicon glyphicon-time',
				url 		: '/pages/registro'
			}];	
		}	


		function _events() {
			$('.btn-menu').unbind('click');
			$('.btn-menu').bind('click', function(e) {
				$target = $(e.currentTarget);

				window.location.href = $target.attr('data-url');
			});
		}

		// Carga e inicializa la vista del menú en el DOM
		function showView() {
			// Cargamos el template del menú del sistema.
			$('#mainMenu').append(_tplMenu({aplicaciones : _application}));
			
			for(var i=0; i<_application.length; i++) {
				// Dejamos como activo el ícono del módulo en el que esta.
				if(_urlActiva === _application[i].url)
					$('#modulo'+_application[i].idModulo).addClass('btn-menu-active');
			}

			$('#mainMenu').css('height', $(window).height());

			// Enlazamos los eventos a la vista.
			_events();
		}

		return {
			showView : showView
		};
	};

	return MenuController;
});
	