define(['app/controller/sistema/Paginador', 'app/controller/sistema/FormUtil', 'text!tpl/sistema/formularioBusqueda.html'], 
	function(Paginador, FormUtil, TplBusqueda) {
	/**
	 * Clase que se encarga de administrar la interfaz del buscador
	 * genérico utilizado en la aplicación, junto con el manejo del formulario
	 * de búsqueda avanzada y el paginador de los listados desplegados.
	 *
	 * @author Francisco Riverosa
	 * @author Cristofer Herrera
	 */
	var BuscadorController = function(options) {
		var _options;
		var _valoresForm;
		var _isShowPopover;
		var _busquedaAvanzadaActiva;
		var _ultimaPagina;
		var _rango;
		var _paginador;		

		_init();

		/**
		 * Inicializa las variables globales.
		 */
		function _init() {
			_options = options;

			_options.showBusquedaAvanzada = (_.isUndefined(_options.showBusquedaAvanzada)) ? true : _options.showBusquedaAvanzada;
			_options.showPaginador = (_.isUndefined(_options.showPaginador)) ? true : _options.showPaginador;

			_valoresForm = {};
			_isShowPopover = false;
			_busquedaAvanzadaActiva = false;
			_ultimaPagina = false;
			
			if(_.isUndefined(_options.rango))
				_rango = 15;
			else 
				_rango = _options.rango;

			_paginador = Paginador(0, _rango, 0);
		}		

		/**
		 * El popover es cerrado y se habilita el botón de búsqueda genérico
		 */
		function _closePopover() {
			_isShowPopover = false;
			$('#formulario-busqueda').popover('hide');
			$('#btnBuscadorPrincipal').removeAttr('disabled');
		}

		/**
		 * El popover se despliega y se activan los eventos asociados a los elementos que contiene.
		 * Los selectores del tipo chosen son activados.
		 * El formulario es seteado con los valores anteriores.
		 */
		function _openPopover() {
			width = parseInt($('#txtBuscar').css('width'), 10) + parseInt($('#btnBusquedaAvanzada').css('width'), 10);
			$('#contenedorFormularioBusquedaAvanzada').css('width', width);
			$('#contenedorFormularioBusquedaAvanzada').css('min-width', width);
			$('#contenedorFormularioBusquedaAvanzada').css('top', 24);

			$('#contenedorFormularioBusquedaAvanzada .popover-content').css('display', 'inherit');

			_options.eventosAvanzada();

			$('#btnCerrarFormularioAvanzada').unbind('click');
			$('#btnCerrarFormularioAvanzada').bind('click', function() {				
				_closePopover();
			});

			$('#btnBuscarAvanzado').unbind('click');
			$('#btnBuscarAvanzado').bind('click', function() {
				_busquedaAvanzadaActiva = true;
				_paginador.init(0, _rango, 0);
				search();
			});

			$('.chosen-select').chosen();
			$('.chosen-select-deselect').chosen({ allow_single_deselect : true });
			
			_focusPrimerElemento();

			FormUtil.setValues('#formBusquedaAvanzada', _valoresForm);
		}

		/**
		 * Carga el template de los botones del paginador
		 * junto con las opciones de Primera y Última página.
		 */
		function _setPaginador() {
			if(_options.showPaginador) {
				var content = 	'<div> '
							+		'<a id="btnPrimeraPagina" style="text-decoration : none; width : 65px;" href="#" accion="primera"><i style="font-style : normal; font-size : 12px;">Primera</i></a>'
							+		'<a id="btnUltimaPagina" style="text-decoration : none; width : 65px;" href="#" accion="ultima"><i style="font-style : normal; font-size : 12px;">&Uacute;ltima</i></a>'
							+	'</div>';

				$('#btnLabelPaginador').parent().toolbar({
					html 			: true,
					hideOnClick 	: true,
					placement 		: 'top',
					cursorPosition 	: false,
					content 		: content
				});
			} else {
				$('#contenedorPaginador').remove();
			}
		}	

		/**
		 * Configura el formulario de búsqueda avanzada para su despliegue
		 */
		function _setAvanzada() {
			// Verificamos si tiene búsqueda avanzada.
			if(_options.showBusquedaAvanzada && !_.isUndefined(_options.contenidoAvanzada)) {
				var width = parseInt($('#txtBuscar').css('width'), 10) + parseInt($('#btnBusquedaAvanzada').css('width'), 10);

				// Se encarga de redimensionar el contenedor del formulario de búsqueda avanzada.
				$(window).resize(function() {
					width = parseInt($('#txtBuscar').css('width'), 10) + parseInt($('#btnBusquedaAvanzada').css('width'), 10);
					$('#contenedorFormularioBusquedaAvanzada').css('width', width);
					$('#contenedorFormularioBusquedaAvanzada').css('min-width', width);
				});

				$('#formulario-busqueda').popover({
					title 		: 'Formulario avanzado de búsqueda',
					placement 	: 'bottom',
					html 		: 'true',
					trigger 	: 'manual',
					content 	: _options.contenidoAvanzada,
					template 	: '<div id="contenedorFormularioBusquedaAvanzada" class="popover" style="border-radius:0px; top: 29px !important;"> <div class="popover-content"  style="display:none;"></div></div>',
				});

				// Eventos para mostrar y ocultar formulario avanzado.			
				_isShowPopover = false;
				$('#btnBusquedaAvanzada').unbind('click');
				$('#btnBusquedaAvanzada').bind('click', function() {
					if(!_isShowPopover) {						
						_isShowPopover = true;
						$('#formulario-busqueda').popover('show');
						$('#btnBuscadorPrincipal').attr('disabled', 'disabled');
					} else {
						_closePopover();
					}
				});

			} else {
				$('#btnBusquedaAvanzada').hide();
			}
		}

		/**
		 * Enlaza los eventos de la vista.
		 */
		function _enlazarEventos() {
			// Antes de desplegar el popover que contiene a los submenus, se verifica
			// la posición en la que se encuentra el scroll para de esta forma asignar
			// la posición correcta a cada uno de los popover.
			$('#formulario-busqueda').on('shown.bs.popover',  function() {
				_openPopover();
			});				

			$('#btnPrevPaginador').unbind('click');
			$('#btnPrevPaginador').bind('click', function() {
				_paginaAnterior();
			});				

			$('#btnNextPaginador').unbind('click');
			$('#btnNextPaginador').bind('click', function() {
				_paginaSiguiente();
			});				

			// Enlazamos la funcion personalizada para el botón nuevo elemento.		
			if(_options.showNuevoElemento) {
				$('#btnNuevoElemento').unbind('click');
				$('#btnNuevoElemento').bind('click', function() {
					$('#btnNuevoElemento').attr('disabled', 'disabled');
					_options.funcionNuevoElemento();
				});						
			}

			// Enlazamos la funcion personalizada para el botón buscar elementos.
			$('#btnBuscadorPrincipal').unbind('click');
			$('#btnBuscadorPrincipal').bind('click', function() {				
				_busquedaAvanzadaActiva = false;
				_paginador.init(0, _rango, 0);
				search();
			});				

			// Enlazamos los popover de los estados.
			if(!_.isUndefined(_options.estados)) {
				//Para cara estado				
				for(var i = 0; i < _options.estados.length; i++) {
					var estado = _options.estados[i];

					// Al vizualizar la descripción de un estado
					$(".simbologia-#{estado.codigo}").unbind('click');
					$(".simbologia-#{estado.codigo}").bind('click' , function(e) {
						var codigo =  $(e.target).attr('codigo');
						// Oculta los demás estados que no posean el codigo del seleccionado.
						for(var i = 0; i < _options.estados.length; i++) 
							if(_options.estados[i].codigo.toString() != codigo)
								$(".simbologia-#{estado.codigo}").popover('hide');							
					});						
				}
			}

			// Enlazamos el toolbar del paginador. 	
			$('#btnLabelPaginador').parent().on('toolbarItemClick', function(e, element) {
				if($(element).attr('accion') === 'primera') 
					_paginaInicio();
				if($(element).attr('accion') === 'ultima') 
					_paginaFinal();
			});				
		}

		/** 
		 * Dirige a la pagina de inicio
		 */
		function _paginaInicio() {
			_paginador.init(0, _rango, 0);
			search();
		}
			

		/** 
		 * Dirige a la pagina de final
		 */
		function _paginaFinal() {
			_ultimaPagina = true
			search();
		}

		/** 
		 * Dirige a la pagina anterior
		 */
		function _paginaAnterior() {
			_paginador.previous()
			search();
		}

		/** 
		 * Dirige a la pagina de siguiente
		 */
		function _paginaSiguiente() {
			_paginador.next()
			search();
		}

		function _focusPrimerElemento() {
			var elemento = $('#formBusquedaAvanzada').find('[tabindex=1]');
			$(elemento).focus();
		}

		/**
		 * Despliega la vista del formulario de búsqueda junto 
		 * con el paginador en el navegador.
		 */
		function showView() {
			$('#'+_options.contenedor).html(_.template(TplBusqueda)({
				titulo : _options.titulo,
				estados : []
			}));

			_setAvanzada();
			_setPaginador();
			_enlazarEventos();
		}

		/**
		 * Si el popover está abierto realiza una búsqueda avanzada, 
		 * si no realiza una búsqueda genérica.
		 */
		function search() {
			if(_isShowPopover) {
				_valoresForm = FormUtil.getValues('#formBusquedaAvanzada');
			} else {
				if(!_busquedaAvanzadaActiva) {
					_valoresForm = FormUtil.getValues(_options.contenidoAvanzada);
					_valoresForm.query = $('#txtBuscar').val();
				}				
			}

			var copiaValores = _.clone(_valoresForm);
			copiaValores.inicio = _paginador.getStart();
			copiaValores.rango = _paginador.getRange();
			copiaValores.ultimaPagina = _ultimaPagina;

			_options.funcionBuscarElementos(copiaValores);

			_closePopover();
		}	

		/**
		 * Retorna una copia de los valores del filtro de búsqueda.
		 */
		function getFiltro() {
			var copiaValores = _.clone(_valoresForm);

			return copiaValores;
		}

		/**
		 * Si el total es 0 muestra el mensaje que no existen datos, 
		 * oculta el menu y el listado
		 */
		function setTotalPaginador(total) {		
			if(total === 0) {
				// Div sin datos
				$('#'+_options.contenedor).find('#sinDatos').show();
				// Paginador
				$('#'+_options.contenedor).find('#contenedorPaginador').hide();
				// Listado
				$('#'+_options.contenedor).next().hide();
			} else {
				$('#'+_options.contenedor).find('#contenedorPaginador').show();
				$('#'+_options.contenedor).find('#sinDatos').hide();
				$('#'+_options.contenedor).next().show();
			}

			// Setea el total
			_paginador.setTotal(total);

			// Si el flag es verdadero, cambia los valores del paginador a la última pagina
			if(_ultimaPagina) {
				_paginador.last();
				_ultimaPagina = false;
			}

			// Habilita los botones
			$('#btnPrevPaginador').removeAttr('disabled');
			$('#btnNextPaginador').removeAttr('disabled');

			if(_paginador.getStart() === _paginador.getEnd() && _paginador.getEnd() === _paginador.getTotal()  && _paginador.getTotal() != 0)
				_paginador.previous();
				
			// Se encuentra al inicio
			if(_paginador.getStart() <= 1)
				$('#btnPrevPaginador').attr('disabled', 'disabled');
			
			// Se encuentra en el fin
			if(_paginador.getEnd() === _paginador.getTotal())
				$('#btnNextPaginador').attr('disabled', 'disabled');
			
			$('#btnLabelPaginador').html(_paginador.label());
		}

		return {
			showView 			: showView,
			search 				: search,
			getFiltro 			: getFiltro,
			setTotalPaginador 	: setTotalPaginador
		};
	};

	return BuscadorController;
});